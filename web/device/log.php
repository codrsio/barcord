<?php
  include('../res/php/config.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Barcord</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#607d8b">
    <link href="../res/css/main.css" rel="stylesheet" />
  </head>

  <?php
  if (!(isset($_GET['device']) && is_numeric($_GET['device']))) {
    header('Location: '.PROTOCOL.HOSTNAME.PATH.'/scan.php');
    exit();
  }
  $submitted = false;
  if (isset($_POST['status']) && is_numeric($_POST['status']) && isset($_POST['log'])) {

    $submitted = true;
    $device = $_GET['device'];

    $stmt = $conn->prepare(SQL_CREATE_LOG) or die ($conn->error);
    $stmt->bind_param('iiisis', $device, $user, $type, $title, $status, $log);

    $title = $_POST['title'];
    $status = $_POST['status'];
    $log = $_POST['log'];
    $type = $_POST['type'];

    $stmt->execute();
    $log_id = $stmt->insert_id;
    $stmt->close();

    header('Location: '.PROTOCOL.HOSTNAME.PATH.'/logs.php?device='.$device);
    exit();

  }
  ?>

  <body class="dark-bg">
  <div class="appicon"></div>
    <form action="?device=<?php echo $_GET['device']; ?>" method="post" class="form center">
      <label for="type">Type</label>
      <select id="type" name="type" required>
        <option value="1" selected><?= LOG_TYPE_1 ?></option>
        <option value="2"><?= LOG_TYPE_2 ?></option>
      </select>
      <label for="title">Title</label>
      <input type="text" name="title" id="title" required placeholder="Title" />
      <label for="status">Status</label>
      <select id="status" name="status" required>
        <option value="" disabled selected>Status</option>
        <option value="1"><?= DEVICE_STATUS_1 ?></option>
        <option value="2"><?= DEVICE_STATUS_2 ?></option>
        <option value="3"><?= DEVICE_STATUS_3 ?></option>
        <option value="4"><?= DEVICE_STATUS_4 ?></option>
        <option value="5"><?= DEVICE_STATUS_5 ?></option>
        <option value="6"><?= DEVICE_STATUS_6 ?></option>
      </select>
      <label for="log">Log</label>
      <textarea id="log" name="log" placeholder="Log"></textarea>
      <button type="submit" class="ghost">Confirm.</button>
      <a href="view.php?device=<?php echo $_GET['device']; ?>" ><button type="button" class="ghost">Return</button></a>
    </form>
  </body>
</html>
