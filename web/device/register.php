<?php
  include('../res/php/config.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Barcord</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#607d8b">
    <link href="../res/css/main.css" rel="stylesheet" />
  </head>

  <?php
  $submitted = false;
  if (isset($_POST['itf']) && is_numeric($_POST['itf']) && isset($_POST['name']) && isset($_POST['notes'])) {

    $submitted = true;
    $itf = $_POST['itf'];
    $bid = trimID($itf);

    $res = $conn->query("SELECT * FROM `barcode` WHERE `id` = '$bid' AND `device_id` IS NULL LIMIT 1");
    if ($res->num_rows == 1) {

      $submitted = true;
      
      // Register Device
      $stmt = $conn->prepare(SQL_CREATE_DEVICE);
      $stmt->bind_param('sis', $name, $type, $notes);

      $name = $_POST['name'];
      if (isset($_POST['type']) && is_numeric($_POST['type'])) {
        $type = $_POST['type'];
      }
      $notes = $_POST['notes'];

      $stmt->execute();
      $device_id = $stmt->insert_id;
      $stmt->close();
      
      // Add Device to Site
      if (DEFAULT_SITE) {
        $site = DEFAULT_SITE;
      }
      if ($site) {
        $stmt = $conn->prepare(SQL_ADD_DEVICE_TO_SITE);
        $stmt->bind_param('ii', $device_id, $site);
        $stmt->execute();
        $stmt->close();
      }

      // Assign Device to Barcode
      $stmt = $conn->prepare(SQL_ASSIGN_BARCODE);
      $stmt->bind_param('ii', $device_id, $bid);

      $stmt->execute();
      $stmt->close();

      header('Location: '.PROTOCOL.HOSTNAME.PATH.'/view.php?device='.$device_id);
      exit();

    }

  }
  ?>

  <body class="dark-bg">
  <div class="appicon"></div>
    <form action="" method="post" class="form center">
      <p class="flex-container"><input type="number" min="1" step="1" name="itf" placeholder="Barcode-ID" value="<?php if (isset($_GET['itf'])) echo htmlspecialchars($_GET['itf']); ?>" /><button class="scan-button frosted" type="button" title="Scan barcode."></button></p>
      <input type="text" name="name" required placeholder="Name" />
      <select name="type">
        <option value="" disabled selected>Type</option>
        <?php
          $stmt_type = $conn->prepare(SQL_GET_DEVICE_TYPES);
          $stmt_type->execute();
          $stmt_type->bind_result($type_id, $type_name, $type_description);
          while ($stmt_type->fetch()) {
            echo '<option value="'.$type_id.'" title="'.$type_description.'">'.$type_name.'</option>'."\r\n";
          }
          $stmt_type->close();
        ?>
      </select>
      <textarea name="notes" placeholder="Notes"></textarea>
      <button type="submit" class="ghost">Register Device.</button>
      <a href="scan.php" ><button type="button" class="ghost">Return</button></a>
    </form>
  </body>
</html>
