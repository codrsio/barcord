<?php
  include('../res/php/config.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Barcord</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#607d8b">
    <link href="../res/css/main.css" rel="stylesheet" />
  </head>

  <?php
  if (!isset($_GET['log']) || !is_numeric($_GET['log'])) {
    header('Location: '.PROTOCOL.HOSTNAME.PATH.'/logs.php');
    exit();
  }

  if (isset($_POST['status']) && is_numeric($_POST['status']) && isset($_POST['log'])) {

    $logid = $_GET['log'];

    $stmt = $conn->prepare(SQL_UPDATE_LOG) or die ($conn->error);
    $stmt->bind_param('isi', $status, $description, $logid);

    $status = $_POST['status'];
    $description = $_POST['log'];

    $stmt->execute();
    $stmt->close();

    header('Location: '.PROTOCOL.HOSTNAME.PATH.'/editLog.php?log='.$logid);
    exit();

  } else {

    $logid = $_GET['log'];

    $stmt = $conn->prepare(SQL_GET_LOG);
    $stmt->bind_param('i', $logid);
    $stmt->execute();

    $stmt->bind_result($id, $device, $user_id, $created, $type, $title, $status, $description);
    $stmt->fetch();
    $stmt->close();

  }
  ?>

  <body class="dark-bg">
    <div class="appicon"></div>
    
    <form action="?log=<?php echo $_GET['log']; ?>" method="post" class="form center">
      <label for="type">Type</label>
      <select id="type" name="type" disabled>
        <option value="1" <?php if ($type == 1) { echo "selected"; } ?>>Issue</option>
        <option value="2" <?php if ($type == 2) { echo "selected"; } ?>>Maintenance Log</option>
      </select>
      <label for="title">Title</label>
      <input type="text" name="title" id="title" disabled placeholder="Title" value="<?php echo htmlspecialchars($title) ?>" />
      <label for="status">Status</label>
      <select id="status" name="status" required>
        <option value="" disabled>Status</option>
        <option value="1" <?php if ($status == 1) { echo "selected"; } ?>><?= DEVICE_STATUS_1 ?></option>
        <option value="2" <?php if ($status == 2) { echo "selected"; } ?>><?= DEVICE_STATUS_2 ?></option>
        <option value="3" <?php if ($status == 3) { echo "selected"; } ?>><?= DEVICE_STATUS_3 ?></option>
        <option value="4" <?php if ($status == 4) { echo "selected"; } ?>><?= DEVICE_STATUS_4 ?></option>
        <option value="5" <?php if ($status == 5) { echo "selected"; } ?>><?= DEVICE_STATUS_5 ?></option>
        <option value="6" <?php if ($status == 6) { echo "selected"; } ?>><?= DEVICE_STATUS_6 ?></option>
      </select>
      <label for="log">Log</label>
      <textarea id="log" name="log" placeholder="Log"><?php echo htmlspecialchars($description); ?></textarea>
      <button type="submit" class="ghost" disabled="true">No Changes</button>
      <a href="logs.php?device=<?php echo $device; ?>" ><button type="button" class="ghost">Return</button></a>
    </form>
     <script>
      submitButton = document.querySelector('button[type="submit"]');

      function changesMade() {
        submitButton.innerHTML = "Save Changes";
        submitButton.disabled = false;
      }

      document.querySelector('select[name="status"]').addEventListener('change', changesMade, false);
      document.querySelector('textarea[name="log"]').addEventListener('keyup', changesMade, false);
    </script>
  </body>
</html>
