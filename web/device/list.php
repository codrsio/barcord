<?php
include('../res/php/config.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Barcord</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#607d8b">
    <link href="../res/css/main.css" rel="stylesheet" />
  </head>

  <?php
  if (isset($_GET['site']) && is_numeric($_GET['site'])) {
    $site = $_GET['site'];
  } else if (DEFAULT_SITE) {
    $site = DEFAULT_SITE;
  } else {
    header('Location: '.PROTOCOL.HOSTNAME.PATH.'/../');
    exit();
  }

  function getSortLink($row) {
    $desc = isset($_GET['sort']) && $_GET['sort'] == $row && isset($_GET['order']) && $_GET['order'] == 'ASC';
    return "?sort=$row&order=" . ($desc ? 'DESC' : 'ASC');
  }

  $sortBy = isset($_GET['sort']) ? $_GET['sort'] : 'name';
  $sortOrder = isset($_GET['order']) ? $_GET['order'] : 'ASC';

  if (!in_array($sortBy, ['name', 'device_type_name'])) {
    $sortBy = 'name';
  }
  if (!in_array($sortOrder, ['ASC', 'DESC'])) {
    $sortOrder = 'DESC';
  }

  $stmt = $conn->prepare(SQL_GET_DEVICES_AT_SITE." ORDER BY $sortBy $sortOrder;") or exit;
  $stmt->bind_param('i', $site);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id, $name, $type_id, $type_name, $notes);

  ?>

  <body class="dark-bg">
  <div class="appicon"></div>
    <table class="table">
      <thead>
        <tr>
          <th>
            <a href="<?= getSortLink('name') ?>">
              Name
            </a>
          </th>
          <th>
            <a href="<?= getSortLink('device_type_name') ?>">
              Type
            </a>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        while ($stmt->fetch()) {
          $stmt_stat = $conn->prepare(SQL_GET_DEVICESTATUS) or die($conn->error);
          $stmt_stat->bind_param('i', $id);
          $stmt_stat->execute();
          $stmt_stat->bind_result($status);
          $stat = "";
          if ($stmt_stat->fetch()) {
            $stat = getLogStatusClass($status);
          }
          $stmt_stat->close();
          echo "<tr class=\"$stat\" onclick=\"window.location = 'view.php?device=$id';\">";
          echo "  <td>$name</td>";
          echo "  <td>$type_name</td>";
          echo "</tr>";
        }
        ?>
      </tbody>
    </table>
    <div class="start">
      <a href="scan.php" ><button type="button" class="ghost">Scan Code</button></a>
      <a href="../" ><button type="button" class="ghost">Return</button></a>
    </div>
  </body>

  <?php
  $stmt->free_result();
  $stmt->close();
  ?>
</html>
