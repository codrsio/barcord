<?php
include('../res/php/config.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Barcord</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#607d8b">
    <link href="../res/css/main.css" rel="stylesheet" />
  </head>

  <?php
  if (!(isset($_GET['device']) && is_numeric($_GET['device']))) {
    header('Location: '.PROTOCOL.HOSTNAME.PATH.'/scan.php');
    exit();
  }

  $device = $_GET['device'];

  function getStatusMsg($s) {
    switch($s) {
      case 1: return DEVICE_STATUS_1;
    	case 2: return DEVICE_STATUS_2;
    	case 3: return DEVICE_STATUS_3;
    	case 4: return DEVICE_STATUS_4;
    	case 5: return DEVICE_STATUS_5;
    	case 6: return DEVICE_STATUS_6;
    }
  }

  function getLogType($t) {
    switch($t) {
      case 1: return LOG_TYPE_1;
    	case 2: return LOG_TYPE_2;
    }
  }

  function getSortLink($row) {
    $dev = $_GET['device'];
    $desc = isset($_GET['sort']) && $_GET['sort'] == $row && isset($_GET['order']) && $_GET['order'] == 'ASC';
    return "?device=$dev&sort=$row&order=" . ($desc ? 'DESC' : 'ASC');
  }

  $sortBy = isset($_GET['sort']) ? $_GET['sort'] : 'created';
  $sortOrder = isset($_GET['order']) ? $_GET['order'] : 'DESC';

  if (!in_array($sortBy, ['created', 'type', 'title', 'status'])) {
    $sortBy = 'created';
  }
  if (!in_array($sortOrder, ['ASC', 'DESC'])) {
    $sortOrder = 'DESC';
  }

  $stmt = $conn->prepare(SQL_GET_LOGS_BY_DEVICE." ORDER BY i.`$sortBy` $sortOrder;") or exit;
  $stmt->bind_param('i', $device);
  $stmt->execute();
  $stmt->bind_result($id, $device_id, $device_name, $user_id, $created, $type, $title, $status, $description);

  ?>

  <body class="dark-bg">
  <div class="appicon"></div>
    <table class="table">
      <thead>
        <tr>
          <!--th>user_id</th-->
          <th>
            <a href="<?= getSortLink('created') ?>">
              Created
            </a>
          </th>
          <th>
            <a href="<?= getSortLink('type') ?>">
              Type
            </a>
          </th>
          <th>
            <a href="<?= getSortLink('title') ?>">
              Title
            </a>
          </th>
          <th>
            <a href="<?= getSortLink('status') ?>">
              Status
            </a>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        while ($stmt->fetch()) {
          $stat = getLogStatusClass($status);
          echo "<tr class=\"$stat\" onclick=\"window.location = 'editLog.php?log=$id';\">";
          #echo "  <td>$user_id</td>";
          echo "  <td>$created</td>";
          echo "  <td>" . getLogType($type) . "</td>";
          echo "  <td>$title</td>";
          echo "  <td>" . getStatusMsg($status) . "</td>";
          echo "</tr>";
        }
        ?>
      </tbody>
    </table>
    <div class="start">
      <a href="view.php?device=<?php echo $_GET['device']; ?>" ><button type="button" class="ghost">Return</button></a>
    </div>
  </body>

  <?php
  $stmt->close();
  ?>
</html>
