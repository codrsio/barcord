<?php
  include('../res/php/config.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Barcord</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#607d8b">
    <link href="../res/css/main.css" rel="stylesheet" />
  </head>

  <?php
  $submitted = false;

  if (isset($_GET['device']) && is_numeric($_GET['device']) && isset($_POST['name']) && isset($_POST['notes'])) {

    $submitted = true;
    $device = $_GET['device'];

    $stmt = $conn->prepare(SQL_UPDATE_DEVICE);
    $stmt->bind_param('sisi', $name, $type, $notes, $device);

    $name = $_POST['name'];
    if (isset($_POST['type']) && is_numeric($_POST['type'])) {
      $type = $_POST['type'];
    }
    $notes = $_POST['notes'];

    $stmt->execute();
    $stmt->close();

  }


  if (isset($_GET['device']) && is_numeric($_GET['device'])) {

    $device = $_GET['device'];

    $stmt = $conn->prepare(SQL_GET_DEVICE);
    $stmt->bind_param('i', $device);
    $stmt->execute();

    $stmt->bind_result($id, $name, $type, $type_name, $notes);
    $stmt->fetch();
    $stmt->close();

  } else {
    header('Location: '.PROTOCOL.HOSTNAME.PATH.'/scan.php');
    exit();
  }
  ?>

  <body class="dark-bg">
  <div class="appicon"></div>
    <form action="" method="post" class="form center">
      <a href="log.php?device=<?php echo $device; ?>" ><button type="button" class="ghost">Add log.</button></a>
      <a href="logs.php?device=<?php echo $device; ?>" ><button type="button" class="ghost">View logs.</button></a>
      <hr />
      <label for="name">Name</label>
      <input type="text" name="name" id="name" required placeholder="Name" value="<?php echo htmlspecialchars($name); ?>" />
      <label for="type">Type</label>
      <select name="type" id="type">
        <option value="" disabled>Type</option>
        <?php
          $stmt_type = $conn->prepare(SQL_GET_DEVICE_TYPES) or die ($conn->error);
          $stmt_type->execute();
          $stmt_type->bind_result($type_id, $type_name, $type_description);
          while ($stmt_type->fetch()) {
            $tag = '<option value="'.$type_id.'" title="'.$type_description.'"';
            if ($type_id == $type) {
              $tag .= ' selected';
            }
            $tag .= '>'.$type_name.'</option>';
            echo $tag;
          }
          $stmt_type->close();
        ?>
      </select>
      <label for="Notes">Notes</label>
      <textarea name="notes" id="notes" placeholder="Notes"><?php echo htmlspecialchars($notes); ?></textarea>
      <button type="submit" class="ghost" disabled="true">No Changes Made</button>
      <a href="list.php" ><button type="button" class="ghost">Return</button></a>
    </form>
    <script>
      submitButton = document.querySelector('button[type="submit"]');
      function changesMade() {
        submitButton.innerHTML = "Save Changes";
        submitButton.disabled = false;
      }
      document.querySelector('input[name="name"]').addEventListener('keyup', changesMade, false);
      document.querySelector('select[name="type"]').addEventListener('change', changesMade, false);
      document.querySelector('textarea[name="notes"]').addEventListener('keyup', changesMade, false);
    </script>
  </body>
</html>
