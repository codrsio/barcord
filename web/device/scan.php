<?php
  include('../res/php/config.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Barcord</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#607d8b">
    <link href="../res/css/main.css" rel="stylesheet" />
    <link href="../res/css/scan.css" rel="stylesheet" />
  </head>

  <?php
  $submitted = false;
  $failed = false;

  if (isset($_GET['itf']) && is_numeric($_GET['itf'])) {

    $submitted = true;
    $itf = $_GET['itf'];

    $stmt = $conn->prepare(SQL_GET_DEVICE_BY_BARCODE);
    $stmt->bind_param('i', $bid);

    $bid = trimID($itf);

    $stmt->execute();

    $stmt->store_result();
    if ($stmt->num_rows == 1) {
      $stmt->bind_result($device);
      $stmt->fetch();

      if ($device) {
        header('Location: '.PROTOCOL.HOSTNAME.PATH.'/view.php?device='.$device);
      } else {
        header('Location: '.PROTOCOL.HOSTNAME.PATH.'/register.php?itf='.$itf);
      }
      exit();
    } else {
      $failed = true;
    }
    $stmt->close();

  }
  ?>

  <body class="dark-bg">
  <div class="appicon"></div>
    <form action="" method="get" class="form scan-js-form center">
      <?php if ($failed) { echo '<div class="error">Requested barcode is not available.</div>'."\n"; } ?>
      <p class="flex-container one">
        <input class="scan-js-input" type="number" min="1" step="1" name="itf" placeholder="Barcode-ID" required autofocus>
        <button class="scan-button scan-js-btn-scanner frosted" type="button" title="Scan barcode."></button>
      </p>
      <button type="submit" class="one frosted">Go</button>
      <a href="../"><button type="button" class="ghost">Return</button></a>
    </form>

    <div class="scan-scanner scan-js-scanner" hidden>
      <div id="interactive" class="viewport"></div>
      <button type="button" class="flat scan-btn scan-js-btn-cancel">Cancel</button>
    </div>

    <script src="../res/js/quagga.min.js"></script>
    <script src="../res/js/scan.js"></script>
  </body>
</html>
