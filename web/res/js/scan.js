(function() {
  'use strict';

  // Class App
  function App() {
    this._scannerInitialized = false;

    this.scanForm = document.querySelector('.scan-js-form');
    this.scanInput = document.querySelector('.scan-js-input');
    this.scanBtnScanner = document.querySelector('.scan-js-btn-scanner');
    this.scanBtnCancel = document.querySelector('.scan-js-btn-cancel');
    this.scanScanner = document.querySelector('.scan-js-scanner');

    this.init();
  }

  App.prototype.constructor = App;

  App.prototype.init = function() {

    this.scanBtnScanner.addEventListener('click', this._onBtnScannerClick.bind(this));
    this.scanBtnCancel.addEventListener('click', this._onBtnCancelClick.bind(this));

    Quagga.onDetected(this._onQuaggaDetected.bind(this));
  };

  App.prototype._handleError = function(err) {
    console.log('[Quagga.init]', err);
    this.cancelScanner();
    alert('Could not open camera.');
  };

  // Event handlers
  App.prototype._onBtnScannerClick = function(evt) {
    this.openScanner();
  };
  App.prototype._onBtnCancelClick = function(evt) {
    this.cancelScanner();
  };
  App.prototype._onQuaggaDetected = function(data) {
    console.log('[Quagga.onDetected]', data);
    this.cancelScanner();
    this.submitForm(data.codeResult.code);
  };

  // ...
  App.prototype.openScanner = function() {
    var self = this;
    this.scanScanner.removeAttribute('hidden');

    Quagga.init({
      'inputStream': {
        'name': 'Live',
        'type': 'LiveStream'
      },
      'decoder': {
        'drawBoundingBox': true,
        'drawScanline': true,
        'readers': ['i2of5_reader']
      }
    }, function(err) {
      if (err) {
        return self._handleError(err);
      }
      self._scannerInitialized = true;
      Quagga.start();
    })
  };

  App.prototype.cancelScanner = function() {
    this.scanScanner.setAttribute('hidden', 'hidden');
    if (this._scannerInitialized) Quagga.stop();
  };

  App.prototype.submitForm = function(itf_value) {
    this.scanInput.value = itf_value;
    this.scanForm.submit();
  };

  window.app = new App();

})();
