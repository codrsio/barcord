<?php

define("DEFAULT_SITE", "1");

define("LOG_TYPE_1", "Issue");
define("LOG_TYPE_2", "Maintenance Log");

define("DEVICE_STATUS_1", "Ok");
define("DEVICE_STATUS_2", "Software Problem");
define("DEVICE_STATUS_3", "Trivial Fix");
define("DEVICE_STATUS_4", "Minor Disruption");
define("DEVICE_STATUS_5", "Critical");
define("DEVICE_STATUS_6", "Needs Immediate Attention");

function getLogStatusClass($status) {
  if ($status == 1) {
    $stat = "ok";
  } else if ($status == 2) {
    $stat = "software";
  } else if ($status > 2) {
    $stat = "critical";
  } else {
    $stat = "";
  }

  return $stat;
}


// TODO: SQL_GET_ACTIONKEY
// TODO: SQL_CREATE_ACTIONKEY
// TODO: SQL_DELETE_ACTIONKEY

// TODO: SQL_GET_USER
// TODO: SQL_CREATE_USER
// TODO: SQL_UPDATE_USER
// TODO: SQL_DELETE_USER

// TODO: SQL_GET_USERS_BY_SITE
define("SQL_CHECK_SITE_ACCESS", "SELECT `permlevel` FROM `user_has_site` WHERE `user_id` = ? AND `site_id` = ? LIMIT 1;");
define("SQL_ADD_USER_TO_SITE", "INSERT INTO `user_has_site` (`user_id`,`site_id`,`permlevel`) VALUES (?,?,?);");
define("SQL_UPDATE_USER_IN_SITE", "UPDATE `user_has_site` SET `permlevel` = ? WHERE `user_id` = ? AND `site_id` = ? LIMIT 1;");
define("SQL_REMOVE_USER_FROM_SITE", "DELETE FROM `user_has_site` WHERE `user_id` = ? AND `site_id` = ? LIMIT 1;");

// TODO: SQL_GET_SITE
// TODO: SQL_GET_SITES_BY_USER
define("SQL_CREATE_SITE", "INSERT INTO `site` (`name`,`public`) VALUES (?,?);");
define("SQL_UPDATE_SITE", "UPDATE `site` SET `name` = ?, `public` = ? WHERE `id` = ? LIMIT 1;");
define("SQL_DELETE_SITE", "DELETE FROM `site` WHERE `id` = ? LIMIT 1;");

define("SQL_CHECK_DEVICE_ACCESS", "SELECT MAX(u.`permlevel`) AS permlevel FROM `site_has_device` d INNER JOIN `user_has_site` u ON u.`site_id` = d.`site_id` WHERE u.`user_id` = ? AND d.`device_id` = ?;");
define("SQL_ADD_DEVICE_TO_SITE", "INSERT INTO `site_has_device` (`device_id`,`site_id`) VALUES (?,?);");
define("SQL_REMOVE_DEVICE_FROM_SITE", "DELETE FROM `site_has_device` WHERE `device_id` = ? AND `site_id` = ? LIMIT 1;");

define("SQL_GET_DEVICE_TYPE", "SELECT `name`, `description` FROM `device_type` WHERE `id` = ? LIMIT 1;");
define("SQL_GET_DEVICE_TYPES", "SELECT `id`, `name`, `description` FROM `device_type`;");
define("SQL_CREATE_DEVICE_TYPE", "INSERT INTO `device_type` (`name`,`description`) VALUES (?,?);");
define("SQL_UPDATE_DEVICE_TYPE", "UPDATE `device_type` SET `name` = ?, `description` = ? WHERE `id` = ? LIMIT 1;");
define("SQL_DELETE_DEVICE_TYPE", "DELETE FROM `device_type` WHERE `id` = ?;");

define("SQL_GET_DEVICE", "SELECT d.`id` AS id, d.`name` AS name, d.`device_type_id` AS device_type_id, t.`name` AS device_type_name, d.`notes` AS notes FROM `device` d LEFT JOIN `device_type` t ON t.`id` = d.`device_type_id` WHERE d.`id` = ? LIMIT 1;");
define("SQL_GET_DEVICES_AT_SITE", "SELECT d.`id` AS id, d.`name` AS name, d.`device_type_id` AS device_type_id, t.`name` AS device_type_name, d.`notes` AS notes FROM `device` d LEFT JOIN `site_has_device` s ON s.`device_id` = d.`id` INNER JOIN `device_type` t ON t.`id` = d.`device_type_id` WHERE s.`site_id` = ? ");
define("SQL_CREATE_DEVICE", "INSERT INTO `device` (`name`,`device_type_id`,`notes`) VALUES (?,?,?);");
define("SQL_UPDATE_DEVICE", "UPDATE `device` SET `name` = ?,`device_type_id` = ?, `notes` = ? WHERE `id` = ? LIMIT 1;");
define("SQL_DELETE_DEVICE", "DELETE FROM `device` WHERE `id` = ? LIMIT 1;");

define("SQL_GET_DEVICESTATUS", "SELECT l.`status` FROM `log` l INNER JOIN `device` d ON d.`id` = l.`device_id` WHERE d.`id` = ? ORDER BY l.`status` DESC");

define("SQL_GET_DEVICE_BY_BARCODE", "SELECT `device_id` FROM `barcode` WHERE `id` = ? LIMIT 1;");
define("SQL_CREATE_BARCODE", "INSERT INTO `barcode` (`created`,`device_id`) VALUES (NOW(),?);");
define("SQL_ASSIGN_BARCODE", "UPDATE `barcode` SET `device_id` = ? WHERE `id` = ? LIMIT 1;");
define("SQL_DELETE_BARCODE", "DELETE FROM `barcode` WHERE `id` = ? LIMIT 1;");

// TODO: SQL_GET_LAST_MAINTENANCE
// TODO: + user_name
define("SQL_GET_LOG", "SELECT id, `device_id`, `user_id`, `created`, `type`, `title`, `status`, `description` FROM `log` WHERE `id` = ?;");
define("SQL_GET_LOGS_BY_DEVICE", "SELECT i.`id` AS id, i.`device_id` AS device_id, d.`name` AS device_name, i.`user_id` AS user_id, i.`created` AS created, i.`type` AS type, i.`title` AS title, i.`status` AS status, i.`description` AS description FROM `log` i INNER JOIN `device` d ON d.`id` = i.`device_id` WHERE i.`device_id` = ?");
define("SQL_GET_LOGS_AT_SITE", "SELECT i.`id` AS id, i.`device_id` AS device_id, d.`name` AS device_name, i.`user_id` AS user_id, i.`created` AS created, i.`type` AS type, i.`title` AS title, i.`status` AS status, i.`description` AS description FROM `log` i INNER JOIN `site_has_device` s ON s.`device_id` = i.`device_id` INNER JOIN `device` d ON d.`id` = i.`device_id` WHERE s.`site_id` = ?;");
define("SQL_CREATE_LOG", "INSERT INTO `log` (`device_id`,`user_id`,`created`,`type`,`title`,`status`,`description`) VALUES (?,?,NOW(),?,?,?,?);");
define("SQL_UPDATE_LOG", "UPDATE `log` SET `status` = ?, `description` = ? WHERE `id` = ? LIMIT 1;");
define("SQL_DELETE_LOG", "DELETE FROM `log` WHERE `id` = ? LIMIT 1;");
?>
