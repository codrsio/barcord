<?php
/// Generates Interleaved 2of5 Barcodes from Integers and outputs them as a png image.
/// @copyright 2016, The Barcord Team. All rights reserved.
///
/// Syntax:
///   itf.png.php?itf=<itf_value>
///              &w=<width>
///              &h=<height>
///              &bar=<bar_width>
///              &barnarrow=<bar_width_narrow>
///              &barwide=<bar_width_wide>
///
///   Parameter         Type     Required  Default
///   --------------------------------------------------
///   itf_value         long     required
///   width             int      optional  '' (auto)      Not recommended: Scales the image and changes the bar_width.
///   height            int(px)  optional  72
///   bar_width         int(px)  optional  4
///   bar_width_narrow  int(px)  optional  <bar_width>
///   bar_width_wide    int(px)  optional  <bar_width>*2
///

// Log errors
ini_set('display_errors', 1);

// Include the ITF class
require 'itf.inc.php';

header('Content-Type: image/png');
// header('Content-Type: text/plain');

// Extract parameters from URL
function getParameter($key, $default = NULL) {
  return isset($_GET[$key]) ? $_GET[$key] : $default;
}

$itf_value = getParameter('itf');

if ($itf_value === NULL) {
  die('itf is required.');
}

$width            = getParameter('w', '');
$height           = getParameter('h', '72');
$bar_width        = getParameter('bar', '4');
$bar_width_narrow = getParameter('barnarrow', $bar_width);
$bar_width_wide   = getParameter('barwide', (string)((int)$bar_width * 2));

// Construct Itf.
$itf = new Itf($itf_value);

// Calculate Itf's real width.
$real_width = $itf->countNarrow() * $bar_width_narrow + $itf->countWide() * $bar_width_wide;

// Generate Ift barcode.
$itf_code = $itf->generate();

// Create image ($real_width x $height)
$img = imagecreate($real_width, $height);

// Colors
$white = imagecolorallocate($img, 255, 255, 255);
$black = imagecolorallocate($img, 0, 0, 0);

$barorspace = true; // True for bar, false for space.
$pos = 0;

foreach ($itf_code as $bar) {
  $color = $barorspace ? $black : $white;
  $step  = $bar == 0 ? $bar_width_narrow : $bar_width_wide;
  imagefilledrectangle($img, $pos, 0, $pos + $step, $height, $color);
  $pos = $pos + $step;
  $barorspace = !$barorspace;
}

// Scale, if <width> was set.
if ($width != '') {
  $scaledimg = imagescale($img, $width, $height);
  imagepng($scaledimg);
} else {
  imagepng($img);
}

imagedestroy($img);

?>
