<?php

class Itf {

  private $digits;

  public function __construct($value) {
    // Explode digits into array
    $digits = str_split($value);
    // Min length of 4
    if (count($digits) < 4) throw new Exception('The itf must be at least four digits long.');
    $digits = array_map(function($n) {
      return (int)$n;
    }, $digits);
    // Prepend a zero if length is odd.
    if (count($digits) % 2 == 0) array_unshift($digits, 0);

    $this->digits = $digits;
  }

  public function toString() {
    return (string)implode($this->digits) . (string)$this->checksum();
  }

  public function __toString() {
    return $this->toString();
  }

  public function countNarrow() {
    return 4 + (3 * (count($this->digits) + 1)) + 2;
  }
  public function countWide() {
    return (2 * (count($this->digits) + 1)) + 1;
  }

  public function generate() {
    $itf = [];
    $startSeq = [0, 0, 0, 0];
    $stopSeq = [1, 0, 0];

    $digits = array_merge($this->digits, [$this->checksum()]);
    $digits = array_map(function($a) {
      return $this->_encodeDigit($a);
    }, $digits);

    foreach ($digits as $i => $digit) {
      if ($i % 2 == 0) {
        $a = $digits[$i];
        $b = $digits[$i + 1];
        $interleaved = $this->_interleave($a, $b);
        $itf = array_merge($itf, $interleaved);
      }
    }

    return array_merge($startSeq, $itf, $stopSeq);
  }

  public function checksum() {
    $sum = 0;
    foreach ($this->digits as $i => $v) {
      if ($i % 2 == 0) {
      $sum += $v * 3;
      } else {
        $sum += $v;
      }
    }
    $mod = $sum % 10;
    return $mod == 0 ? $mod : 10 - $mod;
  }

  private function _encodeDigit($digit) {
    return [
      '0' => [0, 0, 1, 1, 0],
      '1' => [1, 0, 0, 0, 1],
      '2' => [0, 1, 0, 0, 1],
      '3' => [1, 1, 0, 0, 0],
      '4' => [0, 0, 1, 0, 1],
      '5' => [1, 0, 1, 0, 0],
      '6' => [0, 1, 1, 0, 0],
      '7' => [0, 0, 0, 1, 1],
      '8' => [1, 0, 0, 1, 0],
      '9' => [0, 1, 0, 1, 0]
    ][(string)$digit];
  }

  private function _interleave($a, $b) {
    if (count($a) != count($b)) throw new Exception('The length of $a and $b must be equal.');
    $c = [];
    for ($i = 0; $i < count($a); $i++) {
      $c[] = $a[$i];
      $c[] = $b[$i];
    }
    return $c;
  }

}