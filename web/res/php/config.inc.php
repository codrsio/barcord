<?php

$url = parse_url(getenv("CLEARDB_DATABASE_URL"));

define("SERVER", $url["host"]);
define("USERNAME", $url["user"]);
define("PASSWORD", $url["pass"]);
define("DB", substr($url["path"], 1));

/*define("SERVER", 'localhost');
define("USERNAME", 'root');
define("PASSWORD", '');
define("DB", 'barcord');*/

$conn = new mysqli(SERVER, USERNAME, PASSWORD, DB);

define("PROTOCOL", "https://");
define("HOSTNAME", $_SERVER['HTTP_HOST']);
define("PATH", (dirname($_SERVER['PHP_SELF']) == '/' ? '' : dirname($_SERVER['PHP_SELF'])));

if (stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true){
    $redirect = "https://".HOSTNAME.$_SERVER['REQUEST_URI'];
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: $redirect");
}

include('sql.inc.php');

ini_set('display_errors', '1');

function trimID($id) {
  return (int)substr($id, 0, -1);
}

?>