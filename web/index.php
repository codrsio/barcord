<!DOCTYPE html>
<html lang="en">
    <head>
      <title>Barcord</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="theme-color" content="#607d8b">
      <link href="res/css/main.css" rel="stylesheet" />
    </head>
    <body class="img-bg">
      <div class="appicon"></div>
      
      <div class="start center text">
        <p>
          This is where - normally - a beautifully designed log in form would go. Unfortunately that one does not exist (yet).
          Instead we have prepared a demo in order for you to see how amazing everything is going to become.
        </p>
        <a href="device/print.php" title="Print barcodes."><button class="ghost">Print Barcodes</button></a>
        <a href="device/scan.php" title="If you want to scan a barcode assigned to an existing device, please click here."><button class="ghost">Scan Barcode</button></a>
        <a href="device/list.php" title="A list of all registered devices at your site."><button class="ghost">Device List</button></a>
      </div>
    </body>
</html>